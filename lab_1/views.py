from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Hilmi Arista' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000, 6, 7) #TODO Implement this, format (Year, Month, Date)
npm = 1806147003 # TODO Implement this
angkatan = "2018 (QUANTA)"
hobi = "Membaca Novel, Menggambar, Menonton Film"
deskripsi = "Saya adalah mahasiswa Fasilkom semester 3 yang masih berusaha untuk beradaptasi dengan lingkungan perkuliahan"
kuliah = "Universitas Indonesia"
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'angkatan' : angkatan, 'kuliah' : kuliah, 'hobi' : hobi, 'deskripsi' : deskripsi }
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
